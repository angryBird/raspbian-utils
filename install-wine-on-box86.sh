#!/bin/bash

#Anmerkung: Dieses Skript stammt von der Webseite https://ptitseb.github.io/box86/X86WINE.html und wurde von mir leicht abgeändert.

USER_HOME_DIR=$(eval echo "~pi")

if [ $(id -u) = 0 ]; then
    echo "Starten der Installation..."
    cd $USER_HOME_DIR/Downloads
    wget https://dl.winehq.org/wine-builds/debian/dists/buster/main/binary-i386/wine-stable-i386_7.0.0.0~buster-1_i386.deb # NOTE: Replace this link with the version you want
    wget https://dl.winehq.org/wine-builds/debian/dists/buster/main/binary-i386/wine-stable_7.0.0.0~buster-1_i386.deb  # NOTE: Also replace this link with the version you want
    dpkg-deb -xv wine-stable-i386_7.0.0.0~buster-1_i386.deb wine-installer # NOTE: Make sure these dpkg command matches the filename of the deb package you just downloaded
    dpkg-deb -xv wine-stable_7.0.0.0~buster-1_i386.deb wine-installer
    mv $USER_HOME_DIR/Downloads/wine-installer/opt/wine* $USER_HOME_DIR/wine
    rm wine*.deb # clean up
    rm -rf wine-installer # clean up

    # Install shortcuts (make 32bit launcher & symlinks. Credits: grayduck, Botspot)
    echo -e '#!/bin/bash\nsetarch linux32 -L '"$USER_HOME_DIR/wine/bin/wine "'"$@"' | tee -a /usr/local/bin/wine >/dev/null # Create a script to launch wine programs as 32bit only
    #sudo ln -s ~/wine/bin/wine /usr/local/bin/wine # You could aslo just make a symlink, but box86 only works for 32bit apps at the moment
    ln -s $USER_HOME_DIR/wine/bin/wineboot /usr/local/bin/wineboot
    ln -s $USER_HOME_DIR/wine/bin/winecfg /usr/local/bin/winecfg
    ln -s $USER_HOME_DIR/wine/bin/wineserver /usr/local/bin/wineserver
    chmod +x /usr/local/bin/wine /usr/local/bin/wineboot /usr/local/bin/winecfg /usr/local/bin/wineserver

    # These packages are needed for running wine-staging on RPi 4 (Credits: chills340)
    apt install libstb0 -y
    cd $USER_HOME_DIR/Downloads
    wget -r -l1 -np -nd -A "libfaudio0_*~bpo10+1_i386.deb" http://ftp.us.debian.org/debian/pool/main/f/faudio/ # Download libfaudio i386 no matter its version number
    dpkg-deb -xv libfaudio0_*~bpo10+1_i386.deb libfaudio
    cp -TRv libfaudio/usr/ /usr/
    rm libfaudio0_*~bpo10+1_i386.deb # clean up
    rm -rf libfaudio # clean up

    # Boot wine (make fresh wineprefix in ~/.wine )
    wine wineboot
    echo "Die Installation wurde abgeschlossen."
else
    echo "Die Installation konnte nicht durchgeführt werden, weil dieses Skript entweder mit sudo oder als root ausgefuehrt werden muss!"
fi