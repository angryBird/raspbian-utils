#!/bin/bash

echo "Dieses Skript installiert Node für ARMv7"
USER_HOME_DIR=$(eval echo "~pi")

if [ $(id -u) = 0 ]; then
    cd $USER_HOME_DIR/Downloads
    mkdir node_inst
    cd node_inst
    wget https://nodejs.org/dist/v16.14.0/node-v16.14.0-linux-armv7l.tar.xz
    tar -xf node-v16.14.0-linux-armv7l.tar.xz
    rm -f node-v16.14.0-linux-armv7l.tar.xz
    cd node-v16.14.0-linux-armv7l
    mkdir /opt/node
    cp -rf * /opt/node
    cd /opt/node
    chown -R root . && chgrp -R root .
    ln -sf /opt/node/bin/node /usr/local/bin/node
    ln -sf /opt/node/bin/npm /usr/local/bin/npm
    ln -sf /opt/node/bin/npx /usr/local/bin/npx
    rm -rf $USER_HOME_DIR/Downloads/node_inst
    echo "Die Installation wurde abgeschlossen."
else
    echo "Die Installation konnte nicht durchgefuehrt werden, weil dieses Skript entweder mit sudo oder als root ausgefuehrt werden muss!"
fi