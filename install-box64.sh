#!/bin/bash

if [ $(id -u) = 0 ]; then
    echo "Starten der Installation..."
    apt install git build-essential cmake -y
    mkdir build
    cd build
    wget https://github.com/ptitSeb/box64/archive/refs/tags/v0.1.6.tar.gz
    tar -xf v0.1.6.tar.gz
    cd box64-0.1.6
    mkdir build
    cd build
    cmake .. -DRPI4ARM64=1 -DCMAKE_BUILD_TYPE=RelWithDebInfo
    make -j $(nproc)
    make install
    systemctl restart systemd-binfmt
    echo -e "Die Installation ist abgeschlossen.\nNun bitte das System neustarten."
else
    echo "Die Installation konnte nicht durchgefuehrt werden, weil dieses Skript entweder mit sudo oder als root ausgefuehrt werden muss!"
fi