#!/bin/bash

if [ $(id -u) = 0 ]; then
    echo "Starten der Installation..."
    curl -fsSL https://deb.nodesource.com/setup_lts.x | bash -
    apt install -y nodejs npm
    npm install -g --unsafe-perm node-red
    echo "Die Installation von Node-RED ist fertig!"
else
    echo "Die Installation konnte nicht gestartet werden, weil dieses Script entweder mit sudo oder als Root ausgefuehrt werden muss!"
fi
